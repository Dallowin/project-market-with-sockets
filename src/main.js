import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSocketIO from 'vue-socket.io'



const port = 3005

Vue.use(new VueSocketIO({
    debug: true,
    connection: `http://localhost:${port + 1}`,
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },
    options: {
        path: "/socket"
    } 
}))

Vue.prototype.socket = VueSocketIO

Vue.use(VueAxios, axios)
Vue.config.productionTip = false
axios.defaults.baseURL = `http://localhost:${port}`

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')