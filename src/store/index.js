import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        profile: false,
        markets: [],
        tradingActiveSymbol: "BTCUSD"
    },
    mutations: {
        saveProfile(state, payload) {
            state.profile = payload
        },
        saveMarkets(state, payload) {
            state.markets = payload
        },
        changeTradeingSymbol(state, payload) {
            state.tradingActiveSymbol = payload
        }
    },
    actions: {
        async getProfile({ commit }) {
            const { data } = await axios.get("/myprofile", {
                headers: {
                    token: localStorage.getItem("token")
                }
            })

            if (data.success) {
                commit("saveProfile", data.profile)
            } else {
                commit("saveProfile", false)
            }
        }
    },
    modules: {}
})