import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Balance from '../views/Balance'
import Referral from '../views/Referral'
import Codes from '../views/Codes'
import Login from '../views/Login'
import Register from '../views/Register'
import ForgotPassword from '../views/ForgotPassword'
import TokenListing from '../views/TokenListing'
import Trade from '../views/Trade'
import Market from '../views/Market'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/balance',
        name: 'Balance',
        component: Balance
    },
    {
        path: '/referral',
        name: 'Referral',
        component: Referral
    },
    {
        path: '/codes',
        name: 'Codes',
        component: Codes
    },
    {
        path: '/auth/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/auth/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/auth/forgot-password',
        name: 'ForgotPassword',
        component: ForgotPassword
    },
    {
        path: '/token-listing',
        name: 'TokenListing',
        component: TokenListing
    },
    {
        path: '/trade',
        name: 'Trade',
        component: Trade
    },
    {
        path: '/spot/markets/tickers',
        name: 'Market',
        component: Market
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router